import numpy as np
from scipy.integrate import cumtrapz
import pdb

from matplotlib import pyplot as plt
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all') # tidy up any unshown plots

data = np.loadtxt('data/mice2_3094_wide_sn15_prior_allprior0075_split_bias_seed2_15000_nit_200_largescales_SN10_nobiasedprior_good_FT40_0')
ztrue = np.loadtxt('data/z_for_Ian.txt')

za = np.arange(0.07, ztrue.max(), 0.01)

chain_mean = np.ones(data.shape[0])
nz = np.ones_like(data)

for i,chain in enumerate(data):

  nz[i] = chain/cumtrapz(chain, za)[-1]
  chain_mean[i] = cumtrapz(za*nz[i], za)[-1]

n_mean = np.mean(data, axis=0)
residuals = data - n_mean

plt.figure(1, figsize=(4.5, 3.75))
deltaz = np.mean(ztrue) - chain_mean
plt.hist(deltaz, histtype='step')
plt.xlim([-deltaz.max(), deltaz.max()])
plt.axvline(0, color='k')
plt.xlabel('$\Delta z$')
plt.ylabel('Samples')
plt.savefig('plots/deltaz.png', dpi=300, bbox_inches='tight')

plt.figure(2, figsize=(3.75*4.5, 3.75))
plt.violinplot(residuals, za, widths=0.005, showextrema=False)
plt.axhline(0, color='k')
plt.ylabel('Residual Counts')
plt.xlabel('Redshift')
plt.xlim([0.07, ztrue.max()])
plt.ylim([-650,650])
plt.savefig('plots/violinz.png', dpi=300, bbox_inches='tight')

plt.figure(3, figsize=(4.5, 3.75))
plt.hist(ztrue, histtype='step', normed=True, bins=135, zorder=10, color='lime', label='True')
plt.plot(za, nz[0].T, c='k', alpha=0.05, label='Realisations')
plt.plot(za, nz[1:].T, c='k', alpha=0.05)
plt.xlabel('Redshift')
plt.ylabel('$n(z)$')
plt.legend()
plt.savefig('plots/nzs.png', dpi=300, bbox_inches='tight')